//
//  ViewController.swift
//  Tuples
//
//  Created by Aleksey Knysh on 2/16/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    typealias StudentTuple = (name: String, surname: String, gender: String, yearOfBirth: Double, age: Int, averageScore: Double, formOfEducation: String, faculty: String, needAHostel: Bool)?
    
    func printStudent(tuples: StudentTuple) {
        print("Имя - \(tuples?.name ?? "nil")\nФамилия - \(tuples?.surname ?? "nil")\nПол - \(tuples?.gender ?? "nil")\nДата рождения - \(tuples?.yearOfBirth ?? 1.1)\nВозраст - \(tuples?.age ?? 1)\nСредний бал - \(tuples?.averageScore ?? 1.1)\nФорма обучения - \(tuples?.formOfEducation ?? "nil")\nФакультет - \(tuples?.faculty ?? "nil")\nНужда в общежитии - \(tuples?.needAHostel ?? true)\n")
    }
    
    func comparisonOfTheAverageScore(tuples: StudentTuple, tuples1: StudentTuple) {
        if tuples?.averageScore ?? 1.1 < tuples1?.averageScore ?? 1.1 {
            print("Средний бал у \(tuples?.name ?? "nil") \(tuples?.averageScore ?? 1.1) меньше, чем у \(tuples1?.name ?? "nil") \(tuples1?.averageScore ?? 1.1)")
        } else {
            print("Средний бал у \(tuples?.name ?? "nil") \(tuples?.averageScore ?? 1.1) больше, чем у \(tuples1?.name ?? "nil") \(tuples1?.averageScore ?? 1.1)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let student1 = (name: "Федя", surname: "Смышлённый", gender: "мужской", yearOfBirth: 25.06, age: 21, averageScore: 7.6, formOfEducation: "дневная", faculty: "экономический", needAHostel: true)
        
        let student2 = (name: "Макс", surname: "Большой", gender: "мужской", yearOfBirth: 5.03, age: 17, averageScore: 8.8, formOfEducation: "заочная", faculty: "юридический", needAHostel: false)
        
        let student3 = (name: "Юля", surname: "Студентка", gender: "женский", yearOfBirth: 30.12, age: 25, averageScore: 5.6, formOfEducation: "дневная", faculty: "лингвестический", needAHostel: false)
        
        let student4 = (name: "Даша", surname: "Разумная", gender: "женский", yearOfBirth: 3.03, age: 13, averageScore: 6.3, formOfEducation: "дневная", faculty: "исторический", needAHostel: true)
        
        let student5 = (name: "Рома", surname: "Шутник", gender: "мужской", yearOfBirth: 17.02, age: 24, averageScore: 9.2, formOfEducation: "заочная", faculty: "химический", needAHostel: false)
        
        print("Имя - \(student1.name)\nФамилия - \(student1.surname)\nПол - \(student1.gender)\nДата рождения - \(student1.yearOfBirth)\nВозраст - \(student1.age)\nСредний бал - \(student1.averageScore)\nФорма обучения - \(student1.formOfEducation)\nФакультет - \(student1.faculty)\nНужда в общежитии - \(student1.needAHostel)\n")
        
        let arrayStudent = [student1, student2, student3, student4, student5]
        
        arrayStudent.forEach { printStudent(tuples: $0) }
        
        comparisonOfTheAverageScore(tuples: student5, tuples1: student4)
    }
}
